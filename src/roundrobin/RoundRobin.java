package roundrobin;

import java.util.Scanner;

/**
 *
 * @author Fahimeh
 */
public class RoundRobin {

    static void findWaitingTime(int p[], int n, int bt[], int wt[], int q) {
        int r_bt[] = new int[n];
        for (int i = 0; i < n; i++) {
            r_bt[i] = bt[i];
        }

        int t = 0; // زمان فعلی

        // پیمایش فرآیندها را تا زمانی که همه آنها انجام نشوند به صورت چرخشی ادامه می دهیم
        while (true) {
            boolean done = true;

            // تمام فرآیندها را یک به یک به طور مکرر تکرار می کنیم
            for (int i = 0; i < n; i++) {
                // اگر زمان پشت سر هم یک فرآیند بزرگتر از صفر باشد فقط باید بیشتر پردازش شود
                if (r_bt[i] > 0) {
                    done = false; // یک فرایند معلق وجود دارد

                    if (r_bt[i] > q) {
                        // مقدار زمان را افزایش می دهیم نشان می دهد که یک فرآیند چقدر پردازش شده است
                        t += q;
                        r_bt[i] -= q;
                    } // اگر زمان شروع کوچکتر یا مساوی کوانتوم باشد آخرین چرخه برای این فرآیند می باشد
                    else {
                        // مقدار زمان را افزایش می دهیم نشان می دهد که یک فرآیند چقدر پردازش شده است
                        t = t + r_bt[i];

                        // زمان انتظار زمان فعلی منهای زمان استفاده شده توسط این فرآیند است
                        wt[i] = t - bt[i];

                        // هنگامی که فرآیند به طور کامل اجرا می شود زمان باقی مانده آن را صفر  می کنیم
                        r_bt[i] = 0;
                    }
                }
            }

            // اگر همه فرآیندها انجام شدند
            if (done == true) {
                break;
            }
        }
    }

    // روش محاسبه زمان چرخش
    static void findTurnAroundTime(int p[], int n, int bt[], int wt[], int tat[]) {
        // محاسبه زمان چرخش
        // bt[i] + wt[i]
        for (int i = 0; i < n; i++) {
            tat[i] = bt[i] + wt[i];
        }
    }

    // روش محاسبه میانگین زمان
    static void findavgTime(int p[], int n, int bt[], int q) {
        int wt[] = new int[n], tat[] = new int[n];
        int total_wt = 0, total_tat = 0;

        // عملکردی برای یافتن زمان انتظار برای همه فرآیندها
        findWaitingTime(p, n, bt, wt, q);

        // عملکردی برای یافتن زمان چرخش برای همه فرآیندها
        findTurnAroundTime(p, n, bt, wt, tat);

        // نمایش فرآیندها به همراه تمام جزئیات
        System.out.println("Processes " + " Burst time " + " Waiting time " + " Turn around time");

        // کل زمان انتظار و کل زمان چرخش را محاسبه می کنیم
        for (int i = 0; i < n; i++) {
            total_wt = total_wt + wt[i];
            total_tat = total_tat + tat[i];
            System.out.println(" " + (i + 1) + "\t\t" + bt[i] + "\t " + wt[i] + "\t\t " + tat[i]);
        }

        System.out.println("Average waiting time = " + (float) total_wt / (float) n);
        System.out.println("Average turn around time = " + (float) total_tat / (float) n);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter number of processes: ");
        int n = in.nextInt();
        // شماره فرآیندها
        int p[] = new int[n];
        for (int i = 0; i < n; i++) {
            p[i] = i+1;
        }
        // زمان شروع همه فرآیندها
        int b_t[] = new int[n];
        for (int i = 0; i < n; i++) {
        System.out.println("Enter burst time of process" + (i+1));
            b_t[i] = in.nextInt();
        }
        // کوانتوم زمانی
        System.out.println("Enter time quantum: ");
        int q = in.nextInt();
        findavgTime(p, n, b_t, q);
    }
}
